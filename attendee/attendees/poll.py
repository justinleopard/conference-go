import requests
from .models import ConferenceVO

def get_conferences():
    url = "http://monolith:8000/api/conferences"
    response = requests.get(url)
    content = response.json()
    print ("Just polled...")

    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"], # unique identifier
            defaults={"name": conference["name"], "description": conference["description"]}
        )
