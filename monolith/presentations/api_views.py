from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder
from events.models import Conference
from .models import Presentation

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists presentation titles and links for a specified conference id.
    Returns a dictionary with a single key "presentations" which is a list of presentation titles, status names, and URLs.
    """
    if request.method == "GET":
        presentations = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url(),
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]
        return JsonResponse({"presentations": presentations})
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


def api_show_presentation(request, pk):
    """
    Returns details for a Presentation model specified by the pk parameter.
    This should return a dictionary with presenter's name, company name, email, presentation title, synopsis, creation date, status name, and conference name with URL.
    """
    presentation = Presentation.objects.get(id=pk)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
